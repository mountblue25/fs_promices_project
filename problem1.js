const fs = require("fs")

function createNewDirectory(){
    return new Promise((resolve, reject) =>{
        fs.mkdir("Directory", (error) => {
            if (error) {
                reject(error)
            } else {
                resolve("new directory created")
            }
        })
    })
}
function createNewFile(){
    return new Promise((resolve, reject) =>{
        fs.writeFile('./Directory/mynewfile.json', '{"name": "Raju"}', (error) => {
            if (error) {
                reject(error)
            }
            else {
                resolve('File created!')
            }
        })
    })
}

function deleteTheFile(){
    return new Promise((resolve, reject) => {
        fs.unlink('./Directory/mynewfile.json',(error)=>{
            if (error){
                reject(error)
            } else{
                resolve('File deleted!')
            }
        })
    })
}


module.exports = {createNewDirectory, createNewFile, deleteTheFile}

