let {createNewDirectory, createNewFile, deleteTheFile} = require('../problem1')

createNewDirectory()
.then((content) =>{
    console.log(content)
    return createNewFile()
})
.then((content) => {
    console.log(content)
    return deleteTheFile()
})
.then((content) => {
    console.log(content)
})
.catch((error) =>{
    console.log("error is: ", error)
})

